import { Table } from 'antd'
import { Link } from "react-router-dom";
import { ColumnType } from 'antd/lib/table'
import styles from './FileBrowser.module.css'
import * as AiIcons from 'react-icons/ai';
import { NONAME } from 'dns';
import DataModel from '../lib/DataModel';

interface FileBrowserProps {
    path : string,
    dataSource : Array<DataModel.FileEntry>,
    onFileClick: (fileName : string) => void,
}

interface FileProps {
    name : string,
}


function File(props : FileProps) {
    return (
        <b className={styles.file}>{props.name}</b>
    )
}

const exampleData = [
    {
        fileName: '..',
        kind: DataModel.FileKind.UPLINK,
        lastModified: Date.now(),
    },
    {
        fileName: 'public',
        kind: DataModel.FileKind.DIRECTORY,
        lastModified: Date.now(),
    },
    {
        fileName: 'keynotes.txt',
        kind: DataModel.FileKind.REGULAR,
        lastModified: Date.now(),
    },
    {
        fileName: 'Executor.quiz.md',
        kind: DataModel.FileKind.REGULAR,
        lastModified: 44,
    },
]

function FileBrowser(props : FileBrowserProps) {
    const iconSize = '20px';
    const iconColor = '#0066ff'

    function fileexticon(fileName : string) {
        const exts : Array<[string, JSX.Element|string]> = [
            [".quiz.md", <AiIcons.AiOutlineQuestion size={iconSize} color={iconColor}/>],
            [".md", <AiIcons.AiFillFileMarkdown size={iconSize} color={iconColor}/>],
            [".txt", <AiIcons.AiFillFileText size={iconSize} color={iconColor}/>],
            [".tgz", <AiIcons.AiFillFileZip size={iconSize} color={iconColor}/>],
            [".tar", <AiIcons.AiFillFileZip size={iconSize} color={iconColor}/>],
            [".tar.gz", <AiIcons.AiFillFileZip size={iconSize} color={iconColor}/>],
        ]

        for (let pattern of exts) {
            if (fileName.endsWith(pattern[0])) {
                return pattern[1]
            }
        }
        return '?'
    }

    function ricon(kind : DataModel.FileKind, record : DataModel.FileEntry) {
        switch (kind) {
            case DataModel.FileKind.REGULAR:
                return fileexticon(record.fileName)
            case DataModel.FileKind.UPLINK:
                return null;
                //return <AiIcons.AiOutlineArrowLeft size={iconSize} color={iconColor}/>
            case DataModel.FileKind.DIRECTORY:
                // FaFolder
                return <AiIcons.AiFillFolderOpen size={iconSize} color={iconColor}/>
            default:
                return ""
        }
    }


    function rtext(text : string, record : DataModel.FileEntry) {
        if (record.kind == DataModel.FileKind.UPLINK) {
            return <code><Link to={"/browse" + text}>..</Link></code>
        } else if (record.kind == DataModel.FileKind.DIRECTORY) {
            return <code><Link to={text}>{text}</Link></code>
        } else {
            
            let onFileLinkClick = () => {
                props.onFileClick(record.fileName)
            }
            return <code><a onClick={onFileLinkClick}>{text}</a></code>
        }
    }

    function rdate(ts : number) {
        let t = new Date(ts);
        return t.toLocaleString()
    }

    let style = {
        verticalAlign: 'top'
    }

    return (
        <>
        <code>{props.path}</code>
        <Table<DataModel.FileEntry> dataSource={props.dataSource} size="small" style={style} rowKey={"fileName"}>
            <Table.Column<DataModel.FileEntry> render={ricon} width="30px" dataIndex="kind"/>
            <Table.Column<DataModel.FileEntry> title="Name" render={rtext} dataIndex={"fileName"}/>
            <Table.Column<DataModel.FileEntry> title="Last modified" render={rdate}  width="300px" dataIndex="lastModified"/>
        </Table>
        </>
    )
}

export default FileBrowser