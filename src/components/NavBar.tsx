import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import {  CloudOutlined, SettingOutlined} from '@ant-design/icons';
import {Layout, Menu} from 'antd'
const { Header, Content, Footer, Sider } = Layout;

function NavBar() {
    return (
        <Sider 
        
      style={{
        overflow: 'auto',
        height: '100vh',
        position: 'fixed',
        left: 0,
        top: 0,
        bottom: 0,
      }}
        >
         <div className="logo" />
          <Menu theme="dark" mode="inline">
            <Menu.Item key="browse" icon={<CloudOutlined />}>
              <Link to="/browse">Browse repository</Link>
            </Menu.Item>
            <Menu.Item key="debug" icon={<SettingOutlined/>}>
              <Link to="/debug">Debug informations</Link>
            </Menu.Item>
          </Menu>
        </Sider>
    )
}

export default NavBar;