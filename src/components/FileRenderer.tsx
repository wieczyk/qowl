import { Button, Alert, Divider, Radio, Space, Collapse, Pagination } from "antd";
import { useEffect, useRef, useState } from "react";
import ReactMarkdown from 'react-markdown';
import remarkGfm from 'remark-gfm';
import { JsxElement } from "typescript";
import API from '../lib/API';

interface FileRendererProps {
    fileName: string | null,
    path: string | null,
    content: string | null,
}

interface ContentRendererProps {
    path: string,
    content: string,
}

function TextRenderer(props: ContentRendererProps) {
    return (
        <pre>
            {props.content}
        </pre>

    )
}

function UnknownRenderer(props: ContentRendererProps) {
    return (
        <Alert type="error" message="Cannot render this content. Check file extension." />
    )
}

function MdImg(node: any, props: any, path: string) {
    let clientRef = useRef(new API.Client())
    const [img, setImg] = useState<any>();

    const fetchImage = async () => {
        try {
            let imgPath = path + "/" + node["src"];
            const res = await clientRef.current.fsBlobUrlObject(imgPath);
            console.log(res);
            setImg(res);
        } catch (e: any) {
            console.error("Cannot fetch image:", e)
        }
    }

    useEffect(() => {
        fetchImage();
    }, [])

    console.log('node', node)
    console.log("img", props)
    return (
        <img src={img} />
    )
}

function MarkdownRenderer(props: ContentRendererProps) {
    let clientRef = useRef(new API.Client())

    return (
        <ReactMarkdown
            children={props.content}
            components={{
                img: (xnode, xprops) => MdImg(xnode, xprops, props.path),
            }}
        />
    )
}

function If(props: any) {
    if (props.visible) {
        return props.children;
    }
    return null;
}

function QuizEntry(props: any) {
    let [showExplain, setShowExplain] = useState(false);
    let [answer, setAnswer] = useState<null | number>(null);
    let [correct, setCorrect] = useState<null|boolean>(null);

    let result = null;
    if (correct == true) {
        result = (
            <Alert message="Correct!" type="success"/>
        )
    }
    if (correct == false) {
        result = (
            <Alert message="Correct!" type="error"/>
        )
    }

    function onCheck() {
        setCorrect(answer == 0);
        setShowExplain(true);
    }

    let computeAnswers = () => {
        let answers = new Array<JSX.Element>();
        answers.push(<Radio value={0}>{props.validAnswer}</Radio>);
        for (let i = 0; i < props.invalidAnswers.length; ++i) {
            answers.push(<Radio value={i+1}>{props.invalidAnswers[i]}</Radio>);
        }

        answers.sort( (a,b) => 0.5 - Math.random());
        return answers;
    }

    let [answers, _] = useState(computeAnswers());

    return (
        <>
            <MarkdownRenderer path={props.path} content={props.preamble} />
            <b>Question</b>
            <MarkdownRenderer path={props.path} content={props.question} />
            <Radio.Group onChange={(e: any) => setAnswer(e.target.value)}>
                <Space direction="vertical">
                    {answers}
                </Space>
            </Radio.Group>
            <br/>
            <Button onClick={onCheck} disabled={answer == null}>Check</Button>
            <If visible={correct == true}>
                <Alert message="Correct!" type="success"/>
            </If>
            <If visible={correct == false}>
                <Alert message="Incorrect, try again." type="error"/>
            </If>
            <If visible={showExplain}>
            <Collapse>
                <Collapse.Panel key="0" header="explanation">
                    <MarkdownRenderer path={props.path} content={props.explain} />
                </Collapse.Panel>
            </Collapse>
            </If>

        </>
    )
}

class Quiz {
    content: string
    path: string

    elements = new Array<JSX.Element>();

    get length() : number {
        return this.elements.length;
    }

    constructor(content: string, path: string) {
        this.content = content
        this.path = path

        let split = this.analyze()

        if (split.length > 0) {
            let element = (
                <MarkdownRenderer key="qentry" path={this.path} content={split[0].join("\n")} />
            )
            this.elements.push(element)
        }

        for (let i = 1; i < split.length; ++i) {
            let element = (
                <>
                    <Divider />
                    {this.renderQ(i, split[i])}
                </>
            )
            this.elements.push(element)
        }
    }


    renderQ(index : number, lines: Array<string>) {
        let preambleText = new Array<string>();
        let questionText = new Array<string>();
        let explainText = new Array<string>();
        let validAnswer = "";
        let invalidAnswers = new Array<string>();

        let currentText = preambleText;

        for (let i = 0; i < lines.length; ++i) {
            if (lines[i].startsWith("@Question")) {
                currentText = questionText;
                continue;
            }
            if (lines[i].startsWith("@Explain")) {
                currentText = explainText;
                continue;
            }
            if (lines[i].startsWith("@Valid ")) {
                validAnswer = lines[i].substring(6);
                continue;
            }
            if (lines[i].startsWith("@Invalid ")) {
                invalidAnswers.push(lines[i].substring(8))
                continue;
            }

            currentText.push(lines[i]);
        }

        return (
            <QuizEntry
                key={"quizEntry" + index.toString()}
                path={this.path}
                preamble={preambleText.join("\n")}
                question={questionText.join("\n")}
                validAnswer={validAnswer}
                invalidAnswers={invalidAnswers}
                explain={explainText.join("\n")}
            />
        )
    }


    analyze() {
        const splitLines = (str: string) => str.split(/\r?\n/);
        let lines = splitLines(this.content)

        let result = new Array();
        result.push(new Array());

        for (let line of lines) {
            if (line.startsWith("## ")) {
                result.push(new Array())
            }
            result[result.length - 1].push(line)
        }

        return result
    }

    render() {
        return this.elements;
    }
}


function QuizRenderer(props: ContentRendererProps) {
    let q = new Quiz(props.content, props.path);
    let [visibleIndex, setVisibleIndex] = useState(0)

    function onPageChange(index : number) {
        setVisibleIndex(index)
    }

    console.log(q.length);

    return (<>
    {
        q.elements.map((element, index) => (
            <If key={"ifq" + index.toString()} visible={visibleIndex==index+1}>
                {element}
            </If>
        ))
    }
    <Divider/>
    <Pagination showQuickJumper defaultCurrent={1} total={q.length} pageSize={1}  onChange={onPageChange}/>
    </>);
}

function SelectRenderer(fileName: string) {
    if (fileName.endsWith(".txt")) {
        return TextRenderer;
    }
    if (fileName.endsWith(".quiz.md")) {
        return QuizRenderer;
    }
    if (fileName.endsWith(".md")) {
        return MarkdownRenderer;
    }

    return UnknownRenderer;
}


function FileRenderer(props: FileRendererProps) {


    if (props.fileName == null || props.content == null || props.path == null) {
        return null;
    }

    let Renderer = SelectRenderer(props.fileName);

    return (
        <>
            <h1><code>{props.fileName}</code></h1>
            <Renderer path={props.path} content={props.content} />
        </>
    )

}

export default FileRenderer;