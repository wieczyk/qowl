import { Outlet } from "react-router-dom";
import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Layout, Menu } from 'antd'
import 'antd/dist/antd.css';
import FileBrowser from './components/FileBrowser'
import NavBar from './components/NavBar'
import * as GiIcons from "react-icons/gi";
const { Header, Content, Footer, Sider } = Layout;

function App() {
  return (
    <div className="App">
        <Layout hasSider>
          <NavBar />
          <Layout>
            <Header className="site-layout-sub-header-background" style={{ padding: 0 }} />
            <Content style={{ margin: '24px 16px 0' }}>
              <div className="site-layout-background" style={{ marginLeft: 200, padding: 24, minHeight: 360 }}>
                <Outlet/>
              </div>
            </Content>
            <Footer style={{ textAlign: 'center' }}>
              <GiIcons.GiOwl size="20px"/>
              &nbsp; Quiz Owl App
              </Footer>
          </Layout>
        </Layout>
    </div>
  );
}

export default App;
