import React from 'react';
import {
  Navigate,
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import DebugInfoPage from './pages/DebugInfo'
import BrowsePage from './pages/Browse'
import NotFoundPage from './pages/NotFound'

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
    <Routes>
      <Route path="/" element={<App/>}>
        <Route path="browse/*" element={<BrowsePage/>}/>
        <Route path="debug" element={<DebugInfoPage/>} />
        <Route path="*" element={<NotFoundPage/>} />
      </Route>
    </Routes>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
