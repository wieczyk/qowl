import FileBrowser from '../components/FileBrowser'
import { useParams, Routes, Route, useLocation } from "react-router-dom";
import { useEffect, useRef, useState } from 'react';
import API from '../lib/API';
import DataModel from '../lib/DataModel'
import { useForm } from 'antd/lib/form/Form';
import { Divider } from 'antd';
import FileRenderer from '../components/FileRenderer'

function BrowsePage() {
    let [path, setPath] = useState("/")
    let clientRef = useRef(new API.Client());
    let params = useParams();
    let [entries, setEntries] = useState<Array<DataModel.FileEntry>>([])
    let [content, setContent] = useState<[string, string]|null>(null)

    useEffect(() => {
        setPath("/" + params["*"] as string)
    })

    useEffect(() => {
        async function doit() {
            try {
                let result = await clientRef.current.fsLs(path);
                console.log("result", result)
                setEntries(DataModel.createFromReponse(result))
            } catch (e:any) {
                console.error("Cannot fetch fs/ls: ", e)
                if (path != "/") {
                    setPath("/")
                } else {
                    setEntries([])
                }
            }
        }

        doit();
    }, [path])

    useEffect(() => {
        setContent(null)
    }, [path])

    function onFileClick(fileName : string) {
        let fileUrl = path + "/" + fileName;
        console.log("onFileClick", fileUrl)

        async function doit() {
            setContent(null);
            try {
                let result = await clientRef.current.fsTxt(fileUrl)
                setContent([fileName, result])
            } catch (e: any) {
                console.error("Cannot fetch txt: ", e)
            }
        }

        doit()
    }

    let contentFileName = null;
    let contentContent = null;
    if (content != null) {
        contentFileName = content[0];
        contentContent = content[1];
    }

    return (
        <main>
            <h1>Browser</h1>

            <FileBrowser dataSource={entries} path={path} onFileClick={onFileClick}/>

            <Divider/>
            <FileRenderer path={path} fileName={contentFileName} content={contentContent}/>
        </main>
    ) 
}

export default BrowsePage;
