

function NotFoundPage() {
    console.log("Called!")
    return (
        <main>
            <h1>Page not found</h1>
        </main>
    )    
}

export default NotFoundPage;