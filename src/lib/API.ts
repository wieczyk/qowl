import { blob } from "stream/consumers"

namespace API {

    const default_server = 'http://127.0.0.1:8000'

    export class Client {
        serverUrl : string

        fsEndpoint = "/fs"
        fsLsEndpoint = this.fsEndpoint + "/ls"
        fsDownloadEndpoint = this.fsEndpoint + "/raw"
        fsTxtEndpoint = this.fsEndpoint + "/txt"

        constructor(serverUrl = default_server) {
            this.serverUrl = serverUrl
        }

        private makeUrl(endpoint : string, params : any) {
            let baseUrl = new URL(this.serverUrl + endpoint)
            let queryParams = new URLSearchParams(params)
            baseUrl.search = queryParams.toString();
            return baseUrl.toString()
        }

        private async fetchBlob(endpoint : string, params : any, errorValue : any = null) {
            let url = this.makeUrl(endpoint, params)

            try {
                let response = await fetch(url);
                let blob = await response.blob();
                return blob;
            } catch (e : any) {
                console.error("API:", e)
                return errorValue
            }
        }

        private async fetchJson(endpoint : string, params : any, errorValue : any = null) {
            let url = this.makeUrl(endpoint, params)

            try {
                let response = await fetch(url);
                let json = await response.json();
                let status = json["status"];
                if (!status || status != "ok") throw Error("non-ok response");
                let data = json["data"];
                if (!data) throw Error("missing data");
                return json["data"]
            } catch (e : any) {
                console.error("API:", e)
                return errorValue
            }
        }

        ///////////////////////////////////////////////////////////////////////
        // File System API

        public async fsLs(path : string): Promise<any> {
            let result = await this.fetchJson(this.fsLsEndpoint + '/' + path, {}, null);
            return result;
        }

        public async fsTxt(path : string) {
            let result = await this.fetchJson(this.fsTxtEndpoint + '/' + path, {}, null);
            return result;
        }

        public async fsBlob(path : string) {
            let result = await this.fetchBlob(this.fsDownloadEndpoint + '/' + path, {}, null);
            return result;
        }

        public async fsBlobUrlObject(path : string) {
            let result = await this.fsBlob(path);
            console.log('result', result);
            return URL.createObjectURL(result);
        }
    }


}


export default API