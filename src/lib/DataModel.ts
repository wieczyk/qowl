
namespace DataModel {

export enum FileKind {
    REGULAR = 0,
    DIRECTORY = 1,
    OTHER = 2,
    UPLINK = 3,
    QUIZ = 10,
}

export class FileEntry {
    fileName : string
    kind : FileKind
    lastModified: number

    constructor(fileName : string, kind : FileKind, lastModified : number)
    {
        this.fileName = fileName;
        this.kind = kind;
        this.lastModified = lastModified;
    }
}

export function createFromReponse(response : any) : Array<FileEntry>
{
    let is_root = response["is_root"] as boolean;
    let dirs = response["dirs"];
    let files = response["files"];
    let parent = response["parent"] as string;
    console.log("Heh")

    let result = new Array<FileEntry>();

    if (!is_root) {
        result.push(new FileEntry(parent, FileKind.UPLINK, 0))
    }

    for (let entry of dirs) {
        result.push(new FileEntry(entry[0], FileKind.DIRECTORY, entry[1]))
    }

    for (let entry of files) {
        result.push(new FileEntry(entry[0], FileKind.REGULAR, entry[1]))
    }


    return result;
}


}

export default DataModel